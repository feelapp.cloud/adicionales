About
============
The PWA (progressive web application) backend works like a normal application on the mobile. It allows you to adjust the custom style as your requirement. We provide icon size, name, display orientation, colors, etc options to make quickly app format. You get a combination of a native app with the website.


User Guide
============
Blog: https://www.softhealer.com/blog/odoo-2/post/pwa-progressive-web-application-backend-587

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com

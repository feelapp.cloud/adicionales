# See README.rst file on addon root folder for license details

{
    "name": "Sale UX",
    "version": "15.0.1.0.1",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Usability",
    "depends": ["account", "sale"],
    "data": [
        "views/sale_order_view.xml",
        "views/account_move_view.xml",
    ],
    "installable": True,
}

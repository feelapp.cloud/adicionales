from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    list_price = fields.Float(
        tracking=True,
    )
    standard_price = fields.Float(
        tracking=True,
    )

# See README.rst file on addon root folder for license details

{
    "name": "Account UX",
    "version": "15.0.1.0.6",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Accounting",
    "depends": [
        "account",
        "account_journal_general_sequence",
    ],
    "data": [
        "views/account_fiscal_position_view.xml",
        "views/account_move_view.xml",
        "views/account_move_line_view.xml",
        "views/account_payment_view.xml",
        "views/account_tax_view.xml",
        "views/account_bank_statement_view.xml",
        "data/ir_exports_data.xml",
    ],
    "installable": True,
}

from odoo import _, api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    has_duplicated_invoices = fields.Boolean(
        string="Has Duplicated Invoices",
        compute="_compute_duplicated_invoices_count",
        store=False,
    )

    @api.model
    def _search_duplicated_invoices(self, ref, allowed_move_types, partner_id):
        return self.env["account.move"].search(
            [
                ("partner_id", "=", partner_id),
                ("move_type", "in", allowed_move_types),
                ("ref", "=", ref),
            ]
        )

    @api.depends("ref", "move_type", "partner_id")
    def _compute_duplicated_invoices_count(self):
        allowed_move_types = ("in_invoice", "in_refund")
        for invoice in self:
            if all(
                [
                    invoice.ref,
                    invoice.move_type in allowed_move_types,
                    invoice.partner_id,
                ]
            ):
                duplicated_invoices = self._search_duplicated_invoices(
                    invoice.ref, allowed_move_types, invoice.partner_id.id
                )
                invoice.has_duplicated_invoices = (len(duplicated_invoices) - 1) > 0
            else:
                invoice.has_duplicated_invoices = False

    @api.onchange("ref", "move_type", "partner_id")
    def _onchange_bill_reference(self):
        allowed_move_types = ("in_invoice", "in_refund")
        if not all([self.ref, self.move_type in allowed_move_types, self.partner_id]):
            return

        duplicated_invoices = self._search_duplicated_invoices(
            self.ref, allowed_move_types, self.partner_id.id
        )
        if duplicated_invoices:
            self.has_duplicated_invoices = bool(duplicated_invoices)
            return {
                "warning": {
                    "title": _("Duplicated invoices"),
                    "message": _(
                        "Found duplicated invoices with "
                        "the same Bill Reference on this partner."
                    ),
                }
            }

    @api.onchange("ref", "payment_reference")
    def _onchange_payment_reference(self):
        for line in self.line_ids.filtered(
            lambda line: line.account_id.user_type_id.type in ("receivable", "payable")
        ):
            if self.ref == self.payment_reference:
                line.name = self.ref
            else:
                line.name = " - ".join(
                    list(filter(bool, [self.ref, self.payment_reference]))
                )

    def action_open_duplicated_invoices(self):
        self.ensure_one()
        duplicated_invoices = (
            self._search_duplicated_invoices(
                self.ref,
                ("in_invoice", "in_refund"),
                self.partner_id.id,
            )
            - self
        )
        action = self.env.ref("account.action_move_journal_line").read()[0]
        action["context"] = {}
        action["domain"] = [
            ("id", "in", duplicated_invoices.ids),
        ]
        return action

# Copyright 2022 Moduon
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
from odoo import fields, models


class AccountFiscalPositionTax(models.Model):
    _inherit = "account.fiscal.position.tax"

    tax_src_invoice_repartition_tag_ids = fields.Many2many(
        string="Invoice Product Tax Grids",
        related="tax_src_id.invoice_repartition_tag_ids",
    )
    tax_src_refund_repartition_tag_ids = fields.Many2many(
        string="Refund Invoice Product Tax Grids",
        related="tax_src_id.refund_repartition_tag_ids",
    )
    tax_dest_invoice_repartition_tag_ids = fields.Many2many(
        string="Invoice Applied Tax Grids",
        related="tax_dest_id.invoice_repartition_tag_ids",
    )
    tax_dest_refund_repartition_tag_ids = fields.Many2many(
        string="Refund Invoice Applied Tax Grids",
        related="tax_dest_id.refund_repartition_tag_ids",
    )

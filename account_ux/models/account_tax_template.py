from odoo import api, models


class AccountTaxTemplate(models.Model):
    _inherit = "account.tax.template"

    @api.depends("name")
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

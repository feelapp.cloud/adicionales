Improvements on Account UX:
  - Adds search field ``amount_total_signed`` on customer invoices, supplier invoices and account moves (Only decimal separator with ``,``)
  - Adds field ``date`` (Accounting Date) to the tree view on supplier invoices
  - Adds the posibility to search by ``debit`` and ``credit`` fields at the same time on account move lines (Only decimal separator with ``.``)
  - Adds the posibility to search by ``amount`` field on payments. (Only decimal separator with ``,``)
  - Warns the user if the Bill Reference on Supplier invoice exists in other Invoice
  - Adds the Bill Reference and Payment Reference on the Label of Journal Items
  - Adds tax grid info in tax list view and fiscal position form view
  - Adds a confirm warning on 'Reset to New' button on account statements
  - Account Tax Template has the same Display Name as Account Tax

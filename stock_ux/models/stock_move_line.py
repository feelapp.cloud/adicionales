from odoo import api, fields, models


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    product_barcode = fields.Char(
        string="Product Barcode",
        compute="_compute_product_barcode",
        store=True,
        help="Product Barcode when the move line was created",
    )

    @api.depends("product_id")
    def _compute_product_barcode(self):
        for record in self:
            record.product_barcode = record.product_id.barcode

# See README.rst file on addon root folder for license details

{
    "name": "Stock UX",
    "version": "15.0.1.0.1",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Inventory/Inventory",
    "depends": ["stock"],
    "data": [
        "views/stock_move_line_view.xml",
    ],
    "installable": True,
}

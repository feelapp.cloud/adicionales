# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* partner_ux
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 15.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-16 08:29+0000\n"
"PO-Revision-Date: 2022-05-16 08:29+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"


#. module: partner_ux
#: model:ir.model,name:partner_ux.model_res_partner
msgid "Contact"
msgstr "Contacto"

#. module: partner_ux
#: model:res.groups,name:partner_ux.group_allow_partner_removal
msgid "Contact Deletion"
msgstr ""

#. module: partner_ux
#: code:addons/partner_ux/models/res_partner.py:0
#, python-format
msgid "Contact deletion is not allowed. Archive it instead."
msgstr ""

#. module: partner_ux
#: model_terms:ir.ui.view,arch_db:partner_ux.view_partner_form
msgid "Important fields as"
msgstr ""

#. module: partner_ux
#: model:ir.model.fields,field_description:partner_ux.field_res_partner__missing_important_fields
#: model:ir.model.fields,field_description:partner_ux.field_res_users__missing_important_fields
msgid "Missing important fields"
msgstr ""

#. module: partner_ux
#: model_terms:ir.ui.view,arch_db:partner_ux.view_partner_form
msgid "are missing on this partner."
msgstr ""

#. module: partner_ux
#: code:addons/partner_ux/models/res_partner.py:0
#, python-format
msgid ""
"\n"
"                    You are not allowed to delete '%(mname)s' (%(recs)s) records.\n"
"\n"
"                    This operation is allowed for the following groups:\n"
"                    \t- %(category)s/%(group)s\n"
"\n"
"                    Contact your administrator to request access if necessary.\n"
"                    "
msgstr ""

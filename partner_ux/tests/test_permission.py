# Copyright 2022 Moduon
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
from odoo.exceptions import AccessError
from odoo.tests.common import SavepointCase, TransactionCase, new_test_user, tagged

GROUP = "partner_ux.group_allow_partner_removal"


class PermissionCase(SavepointCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.partner = cls.env["res.partner"].create({"name": "test"})

    def test_system_user_can_unlink(self):
        """System user doesn't need the permission to unlink."""
        # System user doesn't have the group
        self.assertFalse(self.env.user.has_group(GROUP))
        # But it's not needed
        self.assertTrue(self.partner.unlink())

    def test_unlink_forbidden(self):
        """User without the group cannot remove contact."""
        user = new_test_user(self.env, "test_user")
        with self.assertRaises(AccessError):
            self.partner.with_user(user).unlink()

    def test_unlink_allowed(self):
        """User with the group can remove contact."""
        user = new_test_user(
            self.env, "test_user", f"base.group_partner_manager,{GROUP}"
        )
        self.partner.with_user(user).unlink()


@tagged("post_install", "-at_install")
class MailCompatibilityCase(TransactionCase):
    def test_installing_activity_view(self):
        """Installing an activity view works as expected."""
        mail = self.env["ir.module.module"].search(
            [("name", "=", "mail"), ("state", "=", "installed")]
        )
        if not mail:
            self.skipTest("`mail` module not installed")
        self.env["ir.ui.view"].create(
            {
                "name": "test view",
                "model": "res.partner",
                "arch": '<activity string="Test"><field name="id"/></activity>',
            }
        )

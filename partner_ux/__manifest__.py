# See README.rst file on addon root folder for license details

{
    "name": "Partner UX",
    "version": "15.0.1.0.2",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Usability",
    "depends": ["base"],
    "data": [
        "security/partner_ux_security.xml",
        "views/res_partner_view.xml",
    ],
    "installable": True,
}
